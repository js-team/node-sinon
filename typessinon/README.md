# Installation
> `npm install --save @types/sinon`

# Summary
This package contains type definitions for sinon (https://sinonjs.org).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sinon.

### Additional Details
 * Last updated: Wed, 10 Jan 2024 09:07:08 GMT
 * Dependencies: [@types/sinonjs__fake-timers](https://npmjs.com/package/@types/sinonjs__fake-timers)

# Credits
These definitions were written by [William Sears](https://github.com/mrbigdog2u), [Nico Jansen](https://github.com/nicojs), [James Garbutt](https://github.com/43081j), [Greg Jednaszewski](https://github.com/gjednaszewski), [John Wood](https://github.com/johnjesse), [Alec Flett](https://github.com/alecf), [Simon Schick](https://github.com/SimonSchick), and [Mathias Schreck](https://github.com/lo1tuma).
